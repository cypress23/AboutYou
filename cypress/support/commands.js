import { Subject } from 'rxjs';
import globalLocators from './locators';

Cypress.Commands.add('visitHomePage', () => {
    cy.visit('/')
})

Cypress.Commands.add('visitPage', (page) => {
    cy.visit('/'+ page)
})

//This is used to save time so i dont have to figure out how to inject cookies and what those cookies are.
Cypress.Commands.add('confirmCookies', () => {
    cy.get(globalLocators.cookiesAgreeButton)
        .contains("Souhlasím")
            .click();
})

Cypress.Commands.add('signIn', (email, password) => {
    cy.get(globalLocators.userAcccountButton).trigger('mouseover')
        .should('be.visible')
            .find(globalLocators.signInAriaLabel).click( {force:true}  )
    cy.get(globalLocators.loginEmailTextField)
        .type(email)
    cy.get(globalLocators.loginPasswordTextFiled)
        .type(password)
    cy.get(globalLocators.submitLogin)
        .click( {force:true}  )
})

Cypress.Commands.add('searchFromBar', (product) => {
    cy.get(globalLocators.searchButton).first()
      .click()
        .type(product)
})

Cypress.Commands.add('selectClothingCategory', (category) => {
    cy.get(globalLocators.clothesHeaderMenuSelector)
        .trigger('mouseover')
            .should('be.visible')
                .contains(category).click()
})

Cypress.Commands.add('changeToMensSection', () => {
    cy.get(globalLocators.genderHeaderSwitch+'[title="Muži"]')
        .click( {force: true} );
    cy.url()
        .should('include', 'male')
    cy.wait(2000) //For some reason this wont work without wait. /could use intercept to wait for call to finish
})

Cypress.Commands.add('checkFavouritesItemIsAddedinHeaderIcon', (amount) => {
    cy.get(globalLocators.headerWishListIcon)
        .children()
            .children()
                .should('have.attr', 'data-basket-amount', amount)
})

Cypress.Commands.add('addFirstItemToFavouritesInSelectedCategory', { prevSubject: "optional" }, (number, category) => {
    if(number){
    cy.selectClothingCategory(category)
    cy.get(globalLocators.productTile)
      .eq(number).find(globalLocators.addToWishListButton)
        .click( {force:true} )
          .children()
            .should('have.attr','fill','#000000')
    }
    else{
        cy.selectClothingCategory(category)
        cy.get(globalLocators.productTile)
          .eq(0).find(globalLocators.addToWishListButton)
            .click( {force:true} )
              .children()
                .should('have.attr','fill','#000000')
    }
})  

Cypress.Commands.add('selectFirstItemFromProductTile', (number) => {
    cy.get(globalLocators.productTileTracker)
      .eq(number)
        .children()
            .click( {force:true} )
})

Cypress.Commands.add('selectSize', (number) => {
    cy.get(globalLocators.sizeSelectorLabel)
        .click( {force:true} )
    cy.get(globalLocators.sizeFlyoutBody) //sometimes sizeOptionList is needed and sometimes sizeList hence tests might fail here for now
        .find('li')
            .eq(number)
                .click()
    cy.get(globalLocators.sizeSelectorSizeSelectedTextLabel)
        .should('be.visible')
})

Cypress.Commands.add('addItemToBasket', () => {
    cy.get(globalLocators.addItemToBasketButton)
        .click()
    cy.get(globalLocators.addToBasketFlyout)
        .should('be.visible')
    cy.get(globalLocators.checkoutButton)
        .should('be.visible')    
})

Cypress.Commands.add('proceedToCheckout', () => {
    cy.get(globalLocators.checkoutButton)
        .click( {force:true} )
})

Cypress.Commands.add('annonymousCheckoutProcess', (email, password) => {
    cy.get(globalLocators.loginFlowWrapper)
        .should('be.visible')
    cy.signIn(email, password)
})

Cypress.Commands.add('setCookies', () => {
    cy.setCookie('is_visitor_returning', '0')
    cy.setCookie('Gender.gender', 'female')
    cy.setCookie('OptanonAlertBoxClosed', '2022-07-27T17:32:36.180Z')
    cy.setCookie('OptanonConsent', 'isGpcEnabled=0&datestamp=Wed+Jul+27+2022+19%3A32%3A36+GMT%2B0200+(Central+European+Summer+Time)&version=6.35.0&isIABGlobal=false&consentId=1da1e4ee-8f4c-4435-8336-c6c297e50706&interactionCount=1&landingPath=NotLandingPage&groups=C0001%3A1%2CC0007%3A1%2CBG775%3A1%2CC0002%3A1%2CC0004%3A1&hosts=H205%3A1%2CH275%3A1%2CH366%3A1%2CH208%3A1%2CH204%3A1%2CH407%3A1%2CH206%3A1%2CH226%3A1%2CH414%3A1%2CH348%3A1%2CH351%3A1%2CH344%3A1%2CH223%3A1%2CH346%3A1%2CH335%3A1%2CH358%3A1%2CH245%3A1%2CH356%3A1%2CH211%3A1%2CH215%3A1%2CH234%3A1%2CH357%3A1%2CH361%3A1%2CH218%3A1%2CH345%3A1%2CH244%3A1%2CH214%3A1%2CH213%3A1%2CH216%3A1%2CH212%3A1%2CH339%3A1%2CH379%3A1&genVendors=fm%3A1%2Cam%3A1%2C')
})