// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'
import 'cypress-mochawesome-reporter/register';
// Hide fetch/XHR requests

//Code bellow filters out xHR requests when debugging tests (for clarity only)
// source Stackoverflow

const app = window.top;
if (!app.document.head.querySelector('[data-hide-command-log-request]')) {
  const style = app.document.createElement('style');
  style.innerHTML =
    '.command-name-request, .command-name-xhr { display: none }';
  style.setAttribute('data-hide-command-log-request', '');

  app.document.head.appendChild(style);
}
const resizeObserverLoopErrRe = /^[^(ResizeObserver loop limit exceeded)]/
Cypress.on('uncaught:exception', (err) => {
    /* returning false here prevents Cypress from failing the test */
    if (resizeObserverLoopErrRe.test(err.message)) {
        return false
    }
})
beforeEach(() => {
    //cy.setCookie('_dc_gtm_UA-49778338-23', '1')
    //cy.setCookie('__cf_bm', '0ALCVC8WVp0Wkej.gus7OxQc9H1YE93MxsKaCya06oU-1658943150-0-AVFfbT2oWd3Kn/NeC8a4iGPAuRRxeN0KQctwRpTvUePhaPpXSioOsOZoFttGmjMYXP1+iLv/iCu4PuMI4UlQ8DRBKujY/VCpofhaLI11SzfG')
    //cy.setCookie('_gid', 'GA1.2.343966991.1658943154')
    //cy.setCookie('ay-ab-test-user-id', '262dbba7-d1e4-4b5a-8882-3ada62d5ada6')
    //cy.setCookie('ay-active-ab-tests', 'ssf_b2_d=c')
    //cy.setCookie('_gcl_au', '1.1.1176476336.1658943153')
    cy.setCookie('is_visitor_returning', '0')
    cy.setCookie('Gender.gender', 'female')
    cy.setCookie('OptanonAlertBoxClosed', '2022-07-27T17:32:36.180Z')
    cy.setCookie('OptanonConsent', 'isGpcEnabled=0&datestamp=Wed+Jul+27+2022+19%3A32%3A36+GMT%2B0200+(Central+European+Summer+Time)&version=6.35.0&isIABGlobal=false&consentId=1da1e4ee-8f4c-4435-8336-c6c297e50706&interactionCount=1&landingPath=NotLandingPage&groups=C0001%3A1%2CC0007%3A1%2CBG775%3A1%2CC0002%3A1%2CC0004%3A1&hosts=H205%3A1%2CH275%3A1%2CH366%3A1%2CH208%3A1%2CH204%3A1%2CH407%3A1%2CH206%3A1%2CH226%3A1%2CH414%3A1%2CH348%3A1%2CH351%3A1%2CH344%3A1%2CH223%3A1%2CH346%3A1%2CH335%3A1%2CH358%3A1%2CH245%3A1%2CH356%3A1%2CH211%3A1%2CH215%3A1%2CH234%3A1%2CH357%3A1%2CH361%3A1%2CH218%3A1%2CH345%3A1%2CH244%3A1%2CH214%3A1%2CH213%3A1%2CH216%3A1%2CH212%3A1%2CH339%3A1%2CH379%3A1&genVendors=fm%3A1%2Cam%3A1%2C')
    //cy.setCookie('_ga', 'GA1.2.1531244712.1658943154')
    //cy.setCookie('__cfruid', '0fdc58e760015692f6261437c7967bc8efaecec3-1658943150')
})
// Alternatively you can use CommonJS syntax:
// require('./commands')