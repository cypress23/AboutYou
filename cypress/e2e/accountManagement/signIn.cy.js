/// <reference types="cypress" />
import globalLocators from '../../support/locators'
import globalVariables from '../../support/variables'

describe('Sign in', () => {
  beforeEach(() => {
    cy.visitHomePage();
  })

it('Customers are able to sign in with valid credentials', () => {
    cy.signIn(globalVariables.email, globalVariables.password)
    //should be something in the lines of success
    //Currently getting blocked by cloudflare
  })

it('Customers are not able to sign in with invalid email (no dot for code', () => {
  cy.signIn(globalVariables.invalidEmail, globalVariables.password)
  cy.get('[data-testid="ErrorInfoText"]').should('include.text', globalVariables.invalidEmailErrorMessage)
  })

it('Customers are not able to sign in with incorrect password', () => {
  cy.signIn(globalVariables.email, globalVariables.invalidPassword)
  //currently using incorrect contains message due to AY BE refusing requests from cypress
  //Correct message in comment next to the incorrect one
  //Due to cloudflare this will always return aan incorrect results
  //cy.get(globalLocators.registerAndLoginButtons).contains('Zkontroluj prosím své údaje.')//('globalVariables.invalidPasswordErrorMessage')
  })
})