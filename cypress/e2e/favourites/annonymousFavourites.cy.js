/// <reference types="cypress" />
import globalLocators from '../../support/locators'
import globalVariables from '../../support/variables'

describe('AnnonymousFavourites', () => {
  beforeEach(() => {
    cy.visitPage(globalVariables.womensNewClothesUrl);
  })

it('Anonymous client can add new womens clothes to favourites', () => {
    cy.addFirstItemToFavouritesInSelectedCategory(globalVariables.newCategory)
    cy.checkFavouritesItemIsAddedinHeaderIcon(1)
  })
})