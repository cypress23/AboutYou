/// <reference types="cypress" />
import selectors from '../support/locators';
import variables from '../support/variables';
import globalVariables from '../../../support/variables'

describe('Womens clothing categories', () => {
  beforeEach(() => {
    cy.visitHomePage();
  })

  it('New womens clothes category is accessible and directs to correct page', () => {
    cy.selectClothingCategory(globalVariables.newCategory)
    cy.url().should('include', globalVariables.womensNewClothesUrl)
  })
})