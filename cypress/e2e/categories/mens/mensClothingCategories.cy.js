/// <reference types="cypress" />
import selectors from '../support/locators';
import variables from '../support/variables';
import globalVariables from '../../../support/variables';
import globalLocators from '../../../support/locators';

describe('Mens clothing categories', () => {
  beforeEach(() => {
    cy.visitHomePage();
    cy.changeToMensSection();
  })

  it('New mens clothes category is accessible and directs to correct page', () => {
    cy.selectClothingCategory(globalVariables.newCategory)
    cy.url().should('include', globalVariables.mensNewClothesUrl)
  })
})