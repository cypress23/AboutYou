import './accountManagement/signIn.cy'
import './categories/mens/mensClothingCategories.cy'
import './categories/womens/womensClothingCategories.cy'
import './favourites/annonymousFavourites.cy'
import './search/search.cy'
import './shopping/checkout.cy'

//To use this rename this file to all.cy.js
//Use this to run all spec files from cypress UI as feature run all files was removed in cypress 10