/// <reference types="cypress" />
import globalVariables from '../../support/variables';
import globalLocators from '../../support/locators';

describe('Checkout process', () => {
  beforeEach(() => {
    cy.visit('/')

  })

  it('Annonymous user checks out a single item from womens New category', () => {
    cy.selectClothingCategory(globalVariables.newCategory)
    cy.url().should('include', globalVariables.womensNewClothesUrl)
    cy.selectFirstItemFromProductTile(0)
    cy.selectSize(0)
    //cy.addItemToBasket() these will fail due to cloudflair
    //cy.proceedToCheckout() these will fail due to cloudflair
    //cy.annonymousCheckoutProcess(globalVariables.email, globalVariables.password 
    //TODO - Continue to successful payment
  })

/*it ('test flyout', () => {
  //cy.visit('https://www.aboutyou.cz/vas-obchod/p/only/prechodna-bunda-tia-4136057')
  cy.selectClothingCategory(globalVariables.newCategory)
  cy.url().should('include', globalVariables.womensNewClothesUrl)
  cy.selectFirstItemFromProductTile(1)
  cy.selectFirstSize(1)
})*/
})