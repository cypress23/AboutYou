/// <reference types="cypress" />
import globalLocators from '../../support/locators'
import globalVariables from '../../support/variables'

describe('Search', () => {
  beforeEach(() => {
    cy.visitHomePage();
  })

  it('Search returns any products', () => {
    cy.searchFromBar("test");
    cy.get(globalLocators.searchSuggestionContainer)
      .children(globalLocators.productSearchSuggestionContainerItem)
        .should('have.length', 3)
  })
})