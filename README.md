# AboutYou

## Dependencies

1) Installed node.js (Can be downloaded either here: https://nodejs.org/en/download/ or your preferable way)

2) Installation of cypress might be required in that case follow: https://docs.cypress.io/guides/getting-started/installing-cypress

3) VPN running and set to Czech Republic (This is done as i dont have a german VPN so i used local Czech version instead)

# Guide bellow is only for Mac/Linux

# Getting started

1)In terminal connect to folder where you want to store this project

2) Clone the project 
    In terminal copy this: git clone https://gitlab.com/cypress23/AboutYou.git

3) Install npm depedencies
    In terminal copy/write this: npm install

## Commands how to open/ run cypress from terminal

    npm run cy:run:<ENV> - will run all tests headlessly
    npm run cy:open<ENV> - will open cypress ui for easier debug etc.

Open terminal with AboutYou folder opened in it

1) npm run cy:open:production (For production environment - Only working example in this project)

2) npm run cy:open:staging (For staging environment - Only here for multi environment work demonstration)

3) npm run cy:run:production (For production environment - Only working example in this project)

4) npm run cy:run:staging (For staging environment - Only here for multi environment work demonstration)

# How to generate report

## After a succesfull test run using commands above run this:
    Make sure you have terminal open with AboutYou folder opened in it
    
1) Open terminal and type/copy commands bellow (Skip if you still have it open from test run)

2) npm run cy:finalize:report

3) Open finder/file explorer and go to AboutYou folder

4) Go to mochawesome-report folder

5) Open "mochawesome.html"

6) Report is displayed

Example of report file can be previewed here: https://ibb.co/cywRL9k

# Bonus: How could we scale this

To scale to a project of bigger size we could split all the commands from /support/commands.js to each feature folder.

As of now all feature folders have own /support folder where we could add a util.js or commands.js file which would store all code we needed.

Similarly as already shown in the code all locators and variables could be moved to /support/variables(locators).

This will allow to have more readable code instead of a huge pile of code in one command.js / variables.js /locators.js
